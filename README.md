# signal-desktop-docker

A simple re-packaging of the official Signal-Messer desktop-client (developed by Openwhispersystems) within a Docker-container, for usage on Linux desktops which du not support Debian-packages (such as, for example, openSUSE).

The Dockerfile pulls the signal-desktop-client from the official sites. The `run.sh`-file opens a local socket to forward the GUI of the client from within the container to the host system.

## Usage

```
$ git checkout https://gitlab.com/jliebers/signal-desktop-docker.git
$ cd signal-desktop-docker
$ docker build -t signal-desktop
$ sh run.sh
```
