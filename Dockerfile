FROM debian:stretch

RUN apt-get update && apt-get install -y apt-utils sudo gnupg2 curl apt-transport-https libx11-xcb-dev libgtk-3-0 && \
    curl -s https://updates.signal.org/desktop/apt/keys.asc | apt-key add - && \
    echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | tee -a /etc/apt/sources.list.d/signal-xenial.list && \
    apt-get update && apt-get install -y --no-install-recommends signal-desktop libasound2 libasound2-plugins 

RUN export uid=1000 gid=100 && \
    mkdir -p /home/signal && \
    echo "signal:x:${uid}:${gid}:Developer,,,:/home/signal:/bin/bash" >> /etc/passwd && \
    echo "signal:x:${uid}:" >> /etc/group && \
    echo "signal ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/signal && \
    chmod 0440 /etc/sudoers.d/signal && \
    chown ${uid}:${gid} -R /home/signal

USER signal
ENV HOME /home/signal
CMD /usr/local/bin/signal-desktop
